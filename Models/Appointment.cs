﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Tema_2.Models
{
    public class Appointment
    {
        public int Id { get; set; }

        [DataType(DataType.Date)]
        public DateTime date { get; set; }
        public string clientName { get; set; }
        public string phone { get; set; }
        public string car { get; set; }
        public string problem { get; set; }
        public int status { get; set; }
        
    }
}
