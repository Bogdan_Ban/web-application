﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tema_2.Data;
using Microsoft.EntityFrameworkCore;


namespace Tema_2.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new Tema_2Context(
                serviceProvider.GetRequiredService<DbContextOptions<Tema_2Context>>()))
            {
                // Look for any movies.
                if (context.Appointment.Any())
                {
                    return;   // DB has been seeded
                }

                context.Appointment.AddRange(
                    new Appointment
                    {
                        date = DateTime.Parse("2020-2-12"),
                        clientName = "Sally",
                        phone = "0724296785",
                        car = "BMW",
                        status = 1
                    },

                    new Appointment
                    {
                        date = DateTime.Parse("2020-6-22"),
                        clientName = "Mitru",
                        phone = "0723456678",
                        car = "Tesla",
                        status = 0
                    },

                    new Appointment
                    {
                        date = DateTime.Parse("2018-5-30"),
                        clientName = "Tomy",
                        phone = "0781826785",
                        car = "Mercedes",
                        status = 1
                    },

                    new Appointment
                    {
                        date = DateTime.Parse("2020-11-12"),
                        clientName = "Cristi",
                        phone = "0735551991",
                        car = "Dacia",
                        status = 0
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
