﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tema_2.DAL;
using Tema_2.Data;
using Tema_2.Models;

namespace Tema_2.Controllers
{
    [Authorize]
    public class AppointmentsController : Controller
    {
        private readonly Tema_2Context _context;

        //modificari
        /*private IAppointmentRepository appRepos;

        public AppointmentsController(Tema_2Context context)
        {
            this.appRepos = new AppointmentRepository(context);
        }*/
        //

        public AppointmentsController(Tema_2Context context)
        {
            _context = context;
        }

        // GET: Appointments
        /*public async Task<IActionResult> Index()
        {
            return View(await _context.Appointment.ToListAsync());
        }*/
        public async Task<IActionResult> Index(string searchString1, string searchString2)
        {
            var apps = from m in _context.Appointment
                         select m;
            //modificare
            //var apps = from m in appRepos.GetApps()
            //           select m;

            if (!String.IsNullOrEmpty(searchString1) && !String.IsNullOrEmpty(searchString2))
            {
                //apps = apps.Where(s => s.date.ToString().Contains(searchString1));
                apps = apps.Where(s => s.date > DateTime.Parse(searchString1) && s.date < DateTime.Parse(searchString2));
            }
            else 
            {
                if (!String.IsNullOrEmpty(searchString1))
                {
                    apps = apps.Where(s => s.date > DateTime.Parse(searchString1));
                }
                else
                {
                    if (!String.IsNullOrEmpty(searchString2))
                    {
                        apps = apps.Where(s => s.date < DateTime.Parse(searchString2));
                    }
                }
            }

            return View(await apps.ToListAsync());
            //modificare
            //return View(await apps.AsQueryable().ToListAsync());

        }

        // GET: Appointments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointment
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appointment == null)
            {
                return NotFound();
            }

            return View(appointment);
        }

            //modificare
        /*public ViewResult Details(int id)
        {
            Appointment app = appRepos.GetAppByID(id);
            return View(app);
        }*/
        //

        // GET: Appointments/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Appointments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,date,clientName,phone,car,problem,status")] Appointment appointment)
        {
            if (ModelState.IsValid)
            {
                 _context.Add(appointment);
                 await _context.SaveChangesAsync();
                 //modificare
                //appRepos.InsertApp(appointment);
                //appRepos.Save();
                //
                return RedirectToAction(nameof(Index));
            }
            return View(appointment);
        }

        // GET: Appointments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointment.FindAsync(id);
            if (appointment == null)
            {
                return NotFound();
            }
            return View(appointment);
        }
        //modifcare
        /*public ActionResult Edit(int id)
        {
            Appointment app = appRepos.GetAppByID(id);
            return View(app);
        }*/
        //

        // POST: Appointments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,date,clientName,phone,car,problem,status")] Appointment appointment)
        {
            if (id != appointment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(appointment);
                    await _context.SaveChangesAsync();
                    //modificare
                    //appRepos.UpdateApp(appointment);
                    //appRepos.Save();
                    //
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AppointmentExists(appointment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(appointment);
        }

        // GET: Appointments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appointment = await _context.Appointment
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appointment == null)
            {
                return NotFound();
            }

            return View(appointment);
        }
        //modifcare
        /*public ActionResult Delete(bool? saveChangesError = false, int id = 0)
        {
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
            }
            Appointment app = appRepos.GetAppByID(id);
            return View(app);
        }*/
        //

        // POST: Appointments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var appointment = await _context.Appointment.FindAsync(id);
            _context.Appointment.Remove(appointment);
            await _context.SaveChangesAsync();
            //modifcare
            //Appointment app = appRepos.GetAppByID(id);
            //appRepos.DeleteApp(id);
            //appRepos.Save();
            //
            return RedirectToAction(nameof(Index));
        }

        private bool AppointmentExists(int id)
        {
            return _context.Appointment.Any(e => e.Id == id);
            //modifcare
            //return appRepos.GetAppByID(id) != null;
            //
        }
    }
}
