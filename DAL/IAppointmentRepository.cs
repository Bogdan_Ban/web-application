﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tema_2.Models;

namespace Tema_2.DAL
{
    public interface IAppointmentRepository : IDisposable
    {
        IEnumerable<Appointment> GetApps();
        Appointment GetAppByID(int appId);
        void InsertApp(Appointment app);
        void DeleteApp(int appId);
        void UpdateApp(Appointment app);
        void Save();
    }
}
