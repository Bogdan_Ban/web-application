﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Tema_2.Models;
using Tema_2.Data;
using Microsoft.EntityFrameworkCore;

namespace Tema_2.DAL
{
    public class AppointmentRepository : IAppointmentRepository, IDisposable
    {
        private Tema_2Context context;

        public AppointmentRepository(Tema_2Context context)
        {
            this.context = context;
        }

        public IEnumerable<Appointment> GetApps()
        {
            return context.Appointment.ToList();
        }

        public Appointment GetAppByID(int id)
        {
            return context.Appointment.Find(id);
        }

        public void InsertApp(Appointment app)
        {
            context.Appointment.Add(app);
        }

        public void DeleteApp(int appID)
        {
            Appointment appointment = context.Appointment.Find(appID);
            context.Appointment.Remove(appointment);
        }

        public void UpdateApp(Appointment app)
        {
            context.Entry(app).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
