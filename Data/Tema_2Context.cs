﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tema_2.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Tema_2.Data
{
    public class Tema_2Context : DbContext
    {
        public Tema_2Context (DbContextOptions<Tema_2Context> options)
            : base(options)
        {
        }

        public DbSet<Tema_2.Models.Appointment> Appointment { get; set; }
    }
}
